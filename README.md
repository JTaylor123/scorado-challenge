This is a star wars themed sudoku solver.

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Using the solver

Type the numbers into the grid to complete the sudoku. 

Press the reset button to delete any values you have inputted, leaving the initial values.

Press solve to solve the sudoku. 

Press the icon button above the grid to change fom light mode to dark mode. 
