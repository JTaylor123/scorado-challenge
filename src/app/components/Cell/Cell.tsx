import { ChangeEventHandler } from 'react';
import { type CellValueChange } from '../../../hooks/useSudoku';
import styles from './Cell.module.scss';
import { useTheme } from '@/context/ThemeContext';

type CellProps = {
    value: number;
    onChange: ChangeEventHandler<HTMLInputElement>;
}

function Cell({ value, onChange }: CellProps) {

    const { isDarkMode } = useTheme();

    return (
        <td className={`${styles.cell} ${isDarkMode ? styles.dark : styles.light}`}>
            <input className={styles.input}
                type="text"
                maxLength={1}
                value={value !== 0 ? value : ''}
                onChange={onChange}
                pattern="[0-9]"
            />
        </td>
    )
}

export default Cell