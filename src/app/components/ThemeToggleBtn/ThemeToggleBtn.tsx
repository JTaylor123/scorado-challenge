'use client'
import { SunIcon, MoonIcon } from '@heroicons/react/24/outline';
import { useTheme } from '../../../context/ThemeContext';
import styles from './ThemeToggleBtn.module.scss';

const ThemeToggleBtn = () => {
  const { isDarkMode, toggleTheme } = useTheme();

  return (
    <button
      aria-label="Toggle Dark Mode"
      className={`${styles.toggleBtn} ${isDarkMode ? styles.dark : styles.light}`}
      onClick={toggleTheme}
    >
      {isDarkMode ? (
        <MoonIcon className={styles.icon} />
      ) : (
        <SunIcon className={styles.icon} />
      )}
    </button>
  );
};

export default ThemeToggleBtn;