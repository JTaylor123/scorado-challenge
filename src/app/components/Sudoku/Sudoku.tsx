'use client'
import Grid from "../Grid/Grid"
import styles from "./Sudoku.module.scss";
import { useSudoku } from "../../../hooks/useSudoku"
import { useTheme } from "@/context/ThemeContext";
import classNames from "classnames";

type SudokuProps = {
    initalGrid: number[][];
}

export function Sudoku({ initalGrid }: SudokuProps) {
    const {
        handleChange, cells, solve, reset
    } = useSudoku(initalGrid)

    const { isDarkMode } = useTheme();

    const classnames = classNames(styles.btn, {[styles.dark]: isDarkMode});
    return (
        <div>
            <Grid cells={cells} onChange={handleChange} />
            <div className="flex justify-center">
                <button type="button" onClick={() => { solve() }} className={classnames}>
                    Solve
                </button>
                <button type="button" onClick={() => { reset() }} className={classnames}>
                    Reset
                </button>
            </div>
            
        </div>
    )
}