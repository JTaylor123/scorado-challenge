'use client'
import Cell from '../Cell/Cell';
import styles from './Grid.module.scss';
import { type CellValueChange } from '../../../hooks/useSudoku';
import { useTheme } from '@/context/ThemeContext';

type GridProps = {
    cells: number[][];
    onChange: CellValueChange;
}

const Grid = ({ cells, onChange }: GridProps) => {

    const { isDarkMode } = useTheme();

    return (
        <table className={`${styles.grid} ${isDarkMode ? styles.dark : styles.light}`}>
            <tbody>
                {
                    [0, 1, 2, 3, 4, 5, 6, 7, 8].map((row, rowIndex) => {
                        return <tr key={rowIndex} className={styles.gridRow}>
                            {[0, 1, 2, 3, 4, 5, 6, 7, 8].map((col, colIndex) => {
                                return <Cell key={col} value={cells[row][col]} onChange={e => onChange(col, row, e.target.value)} />
                            })}

                        </tr>

                    })
                }
            </tbody>
        </table>
    );
};

export default Grid;
