import { Sudoku } from "./components/Sudoku/Sudoku";
import { Cedarville_Cursive } from "next/font/google";
import ThemeToggleBtn from "./components/ThemeToggleBtn/ThemeToggleBtn";
import { useTheme } from "../context/ThemeContext";


const initalGrid = [
  [0, 0, 0, 0, 0, 0, 7, 2, 0],
  [9, 0, 0, 0, 4, 0, 0, 0, 1],
  [8, 0, 0, 7, 0, 0, 0, 4, 0],
  [0, 5, 0, 0, 3, 0, 0, 0, 8],
  [0, 6, 0, 1, 0, 0, 5, 0, 2],
  [0, 2, 0, 0, 7, 0, 0, 0, 0],
  [3, 0, 0, 0, 1, 0, 0, 0, 5],
  [0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 8, 0, 0, 0, 6, 0, 0, 4],
];

export default function Home() {

  
  return (
      <main className="main">
        <div className="container flex-column align-center justify-center">
          <ThemeToggleBtn/>
          
          <Sudoku initalGrid={initalGrid} />

        </div>
      </main>
    
    
  );
}
