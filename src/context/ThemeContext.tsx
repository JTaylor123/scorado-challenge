'use client';

import { ReactNode, createContext, useCallback, useContext, useEffect, useState } from 'react';

export const ThemeContext = createContext({isDarkMode: false, toggleTheme: () => {}});

export const ThemeProvider = ({ children}:{children: ReactNode}) => {
  const [isDarkMode, setIsDarkMode] = useState(false);

  const toggleTheme = useCallback(() => {
    setIsDarkMode(value => !value);
  }, [setIsDarkMode]);

  useEffect(() => {
    document.documentElement.classList.toggle('dark', isDarkMode);
  }, [isDarkMode]);

  return (
    <ThemeContext.Provider value={{ isDarkMode, toggleTheme }}>
      {children}
    </ThemeContext.Provider>
  );
};

export const useTheme = () => useContext(ThemeContext);