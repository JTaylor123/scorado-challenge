import { useCallback, useEffect, useState } from "react";
import { solver } from "@/hooks/solver";

export type CellValueChange = ReturnType<typeof useSudoku>['handleChange'];

export function useSudoku(initialCells: number[][]) {
    const [cells, setCells] = useState(initialCells);

    const solve = useCallback(() => {
        const solvedCells = solver(cells);
        setCells(solvedCells);
    }, [cells, setCells])

    const handleChange = useCallback((col: number, row: number, value: string) => {

        if(value === ''){
            value = '0';
        }

        if (value.length <= 1 && value.match(/^[0-9]$/)) {
            const cellValue = Number(value);

            if (Number.isInteger(cellValue)) {
                setCells(c => {
                    const newCells = structuredClone(c);
                    newCells[row][col] = cellValue

                    return newCells
                });
            }
        }
    }, [setCells]);

    const reset = useCallback(() =>{
        setCells(initialCells);
    },[setCells, initialCells]);

    return { handleChange, cells, solve, reset }
}