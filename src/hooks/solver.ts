type TBoard = number[][];
type TEmptySpot = [number, number];


const nextEmptySpot = (board: TBoard): TEmptySpot => {
  for (var i = 0; i < 9; i++) {
    for (var j = 0; j < 9; j++) {
      if (board[i][j] === 0) return [i, j];
    }
  }
  return [-1, -1];
};

const checkRow = (board: TBoard, row: number, value: number): boolean => {
  for (let i = 0; i < board[row].length; i++) {
    if (board[row][i] === value) {
      return false;
    }
  }

  return true;
};

const checkColumn = (board: TBoard, column: number, value: number): boolean => {
  for (let i = 0; i < board.length; i++) {
    if (board[i][column] === value) {
      return false;
    }
  }

  return true;
};

const checkSquare = (
  board: TBoard,
  row: number,
  column: number,
  value: number
): boolean => {
  const boxRow: number = Math.floor(row / 3) * 3;
  const boxCol: number = Math.floor(column / 3) * 3;

  for (var r = 0; r < 3; r++) {
    for (var c = 0; c < 3; c++) {
      if (board[boxRow + r][boxCol + c] === value) return false;
    }
  }

  return true;
};

const checkValue = (
  board: TBoard,
  row: number,
  column: number,
  value: number
): boolean => {
  if (
    checkRow(board, row, value) &&
    checkColumn(board, column, value) &&
    checkSquare(board, row, column, value)
  ) {
    return true;
  }

  return false;
};

const solveSudoku = (board: TBoard): TBoard => {
  const emptySpot = nextEmptySpot(board);
  const row = emptySpot[0];
  const col = emptySpot[1];

  if (row === -1 && col === -1) {
    // board is full, return the solution
    return board;
  }

  for (let num = 1; num <= 9; num++) {
    if (checkValue(board, row, col, num)) {

      board[row][col] = num;
      solveSudoku(board);
    }
  }

  if (nextEmptySpot(board)[0] !== -1) board[row][col] = 0;

  return board;
};

export const solver = (board: TBoard) => solveSudoku(structuredClone(board));